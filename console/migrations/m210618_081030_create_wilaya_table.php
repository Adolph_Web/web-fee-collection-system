<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%wilaya}}`.
 */
class m210618_081030_create_wilaya_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%wilaya}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'mkoa_id' => $this->integer()->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);


        // creates index for column `mkoa_id`
        $this->createIndex(
            'idx-wilaya-mkoa_id',
            'wilaya',
            'mkoa_id'
        );


        $this->addForeignKey(
            'fk-wilaya-mkoa_id',
            'wilaya',
            'mkoa_id',
            'mkoa',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%wilaya}}');
    }
}
