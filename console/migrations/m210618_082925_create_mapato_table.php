<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mapato}}`.
 */
class m210618_082925_create_mapato_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mapato}}', [
            'id' => $this->primaryKey(),
            'vocha_id' => $this->integer()->notNull(),
            'kiasi' => $this->decimal(10,2),
            'mlipaji_id' => $this->integer()->notNull(),
            'chanzo_cha_malipo' => $this->string(200),
            'mfanyakazi_id' => $this->integer(),
            'mtaa_id' => $this->integer(),
            'status' => $this->integer(),

        ]);




        // creates index for column `vocha_id`
        $this->createIndex(
            'idx-mapato-vocha_id',
            'mapato',
            'vocha_id'
        );


        $this->addForeignKey(
            'fk-mapato-vocha_id',
            'mapato',
            'vocha_id',
            'vocha',
            'id',
            'CASCADE'
        );




        // creates index for column `mlipaji_id`
        $this->createIndex(
            'idx-mapato-mlipaji_id',
            'mapato',
            'mlipaji_id'
        );


        $this->addForeignKey(
            'fk-mapato-mlipaji_id',
            'mapato',
            'mlipaji_id',
            'mlipaji',
            'id',
            'CASCADE'
        );




        // creates index for column `mfanyakazi_id`
        $this->createIndex(
            'idx-mapato-mfanyakazi_id',
            'mapato',
            'mfanyakazi_id'
        );


        $this->addForeignKey(
            'fk-mapato-mfanyakazi_id',
            'mapato',
            'mfanyakazi_id',
            'mfanyakazi',
            'id',
            'CASCADE'
        );




        // creates index for column `mtaa_id`
        $this->createIndex(
            'idx-mapato-mtaa_id',
            'mapato',
            'mtaa_id'
        );


        $this->addForeignKey(
            'fk-mapato-mtaa_id',
            'mapato',
            'mtaa_id',
            'mtaa',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mapato}}');
    }
}
