<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vocha}}`.
 */
class m210618_082852_create_vocha_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vocha}}', [
            'id' => $this->primaryKey(),
            'kumbukumbu_namba' => $this->string(200)->notNull(),
            'mwezi' => $this->char(2),
            'mwaka' => $this->char(4),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vocha}}');
    }
}
