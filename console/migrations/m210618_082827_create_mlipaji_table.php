<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mlipaji}}`.
 */
class m210618_082827_create_mlipaji_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mlipaji}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'chanzo_cha_mapato_id' => $this->integer()->notNull(),
            'mtaa_id' => $this->integer()->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);

        // creates index for column `chanzo_cha_mapato_id`
        $this->createIndex(
            'idx-mlipaji-chanzo_cha_mapato_id',
            'mlipaji',
            'chanzo_cha_mapato_id'
        );


        $this->addForeignKey(
            'fk-mlipaji-chanzo_cha_mapato_id',
            'mlipaji',
            'chanzo_cha_mapato_id',
            'chanzo_cha_mapato',
            'id',
            'CASCADE'
        );


        // creates index for column `mtaa_id`
        $this->createIndex(
            'idx-mlipaji-mtaa_id',
            'mlipaji',
            'mtaa_id'
        );


        $this->addForeignKey(
            'fk-mlipaji-mtaa_id',
            'mlipaji',
            'mtaa_id',
            'mtaa',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mlipaji}}');
    }
}
