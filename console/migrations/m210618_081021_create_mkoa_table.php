<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mkoa}}`.
 */
class m210618_081021_create_mkoa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mkoa}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mkoa}}');
    }
}
