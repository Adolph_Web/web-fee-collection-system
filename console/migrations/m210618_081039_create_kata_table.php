<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kata}}`.
 */
class m210618_081039_create_kata_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%kata}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'wilaya_id' => $this->integer()->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);
        // creates index for column `wilaya_id`
        $this->createIndex(
            'idx-kata-wilaya_id',
            'kata',
            'wilaya_id'
        );


        $this->addForeignKey(
            'fk-kata-wilaya_id',
            'kata',
            'wilaya_id',
            'wilaya',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%kata}}');
    }
}
