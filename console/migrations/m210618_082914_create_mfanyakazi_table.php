<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mfanyakazi}}`.
 */
class m210618_082914_create_mfanyakazi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mfanyakazi}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'simu' => $this->char(13),
            'kazi' => $this->string(200)->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mfanyakazi}}');
    }
}
