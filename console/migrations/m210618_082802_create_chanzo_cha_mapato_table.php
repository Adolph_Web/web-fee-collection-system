<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chanzo_cha_mapato}}`.
 */
class m210618_082802_create_chanzo_cha_mapato_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chanzo_cha_mapato}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chanzo_cha_mapato}}');
    }
}
