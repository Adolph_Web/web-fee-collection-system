<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mtaa}}`.
 */
class m210618_081053_create_mtaa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mtaa}}', [
            'id' => $this->primaryKey(),
            'jina' => $this->string(200)->notNull(),
            'kata_id' => $this->integer()->notNull(),
            'maker' => $this->string(200),
            'maker_time' => $this->dateTime()
        ]);

        // creates index for column `kata_id`
        $this->createIndex(
            'idx-mtaa-kata_id',
            'mtaa',
            'kata_id'
        );


        $this->addForeignKey(
            'fk-mtaa-kata_id',
            'mtaa',
            'kata_id',
            'kata',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mtaa}}');
    }
}
