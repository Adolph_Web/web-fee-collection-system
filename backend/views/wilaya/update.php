<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Wilaya */

$this->title = Yii::t('app', 'Update Wilaya: {name}', [
    'name' => $model->jina,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wilaya'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wilaya-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
