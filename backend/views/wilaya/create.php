<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Wilaya */

$this->title = Yii::t('app', 'Ingiza Wilaya mpya');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wilaya'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wilaya-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
