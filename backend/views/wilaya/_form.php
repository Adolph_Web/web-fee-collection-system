<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Wilaya */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wilaya-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <div class="panel panel-primary">
                <div class="row">
                    <div class="col-md-12">

                        <?= $form->field($model, 'jina')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'mkoa_id')->dropDownList(\backend\models\Mkoa::getAll(),['prompt' => '--Chagua--']) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Weka'), ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
        <?php ActiveForm::end(); ?>


</div>
