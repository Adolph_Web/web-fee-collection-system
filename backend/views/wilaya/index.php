<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\Wilayaearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wilaya');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wilaya-index">



    <p>
        <?= Html::a(Yii::t('app', 'Ingiza Wilaya mpya'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= \sdelfi\datatables\DataTables::widget([
        'dataProvider' => $dataProvider,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return [
                'style' => "cursor: pointer",
                'data-id' => $model->id
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'jina',
            [
                'attribute' =>  'mkoa_id',
                'value' => function($model){
                    return $model->mkoa->jina;
                }
            ],
            'maker',
            'maker_time',

            ['class' => 'yii\grid\ActionColumn','header' => 'Actions'],
        ],
        'clientOptions' => [
            "lengthMenu"=> [[20,-1], [20,Yii::t('app',"All")]],
            "info"=>false,
            "responsive"=>true,
            "dom"=> 'lfTrtip',
            "tableTools"=>[
                "aButtons"=> [
                    [
                        "sExtends"=> "copy",
                        "sButtonText"=> Yii::t('app',"Copy to clipboard")
                    ],[
                        "sExtends"=> "csv",
                        "sButtonText"=> Yii::t('app',"Save to CSV")
                    ],[
                        "sExtends"=> "xls",
                        "oSelectorOpts"=> ["page"=> 'current']
                    ],[
                        "sExtends"=> "pdf",
                        "sButtonText"=> Yii::t('app',"Save to PDF")
                    ],[
                        "sExtends"=> "print",
                        "sButtonText"=> Yii::t('app',"Print")
                    ],
                ]
            ]
        ],
//        'bordered' => false,
    ]); ?>

    <?php
    $this->registerJs("

    $('td').click(function (e) {
        var id = $(this).closest('tr').data('id');
        if(e.target == this)
            location.href = '" . Url::to(['view']) . "&id=' + id;
    });

");?>

    <?php Pjax::end(); ?>

</div>
