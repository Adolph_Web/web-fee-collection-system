<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChanzoChaMapato */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chanzo-cha-mapato-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-body">

    <?= $form->field($model, 'jina')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'kiasi')->textInput(['maxlength' => true]) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>





    <?php ActiveForm::end(); ?>

</div>
