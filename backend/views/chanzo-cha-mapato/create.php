<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ChanzoChaMapato */

$this->title = Yii::t('app', 'Ingiza Chanzo kipya Cha Mapato');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chanzo Cha Mapato'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chanzo-cha-mapato-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
