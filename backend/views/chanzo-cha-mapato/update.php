<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ChanzoChaMapato */

$this->title = Yii::t('app', 'Update Chanzo Cha Mapato: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chanzo Cha Mapatos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="chanzo-cha-mapato-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
