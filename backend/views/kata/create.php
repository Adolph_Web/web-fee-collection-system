<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Kata */

$this->title = Yii::t('app', 'Ingiza Kata mpya');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kata'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kata-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
