<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="<?=$assetDir?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">WEB-SRCS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    ['label' => 'LOCATIONS', 'header' => true],
                    ['label' => 'Mikoa', 'icon' => 'fas fa-circle text-yellow', 'url' => ['mkoa/index']],
                    ['label' => 'Wilaya', 'icon' => 'fas fa-circle text-yellow', 'url' => ['wilaya/index']],
                    ['label' => 'Kata', 'icon' => 'fas fa-circle text-yellow', 'url' => ['kata/index']],
                    ['label' => 'Mtaa', 'icon' => 'fas fa-circle text-yellow', 'url' => ['mtaa/index']],

                    ['label' => 'TRANSACTIONS', 'header' => true],
                    ['label' => 'Vocha', 'icon' => 'fas fa-circle text-yellow', 'url' => ['vocha/index']],
                    ['label' => 'Mapato', 'icon' => 'fas fa-circle text-yellow', 'url' => ['mapato/index']],
                    ['label' => 'Walipaji', 'icon' => 'fas fa-circle text-yellow', 'url' => ['mlipaji/index']],

                    ['label' => 'AGENTS', 'header' => true],
                    ['label' => 'Mawakala', 'icon' => 'fas fa-circle text-yellow', 'url' => ['mfanyakazi/index']],
                    ['label' => 'Vyanzo vya mapato', 'icon' => 'fas fa-circle text-yellow', 'url' => ['chanzo-cha-mapato/index']],
                    [
                        "label" =>Yii::t('app','Access Management'),
                        "url" =>  "#",
                        //  'visible' => yii::$app->user->can('manageUser'),
                        'icon' => 'lock text-red',
                        "items" => [

                            [
                                //'visible' => (Yii::$app->user->identity->username == 'admin'),
                                "label" => "Users",
                                "url" => ["/user/index"],
                                "icon" => "user text-red",
                            ],

                            [
                                'visible' => (Yii::$app->user->identity->username == 'admin'),
                                'label' => Yii::t('app', 'Manager Permissions'),
                                'url' => ['/auth-item/index'],
                                'icon' => 'lock text-red',
                            ],
                            [
                                //'visible' => (Yii::$app->user->identity->username == 'admin'),
                                'label' => Yii::t('app', 'Manage User Roles'),
                                'url' => ['/role/index'],
                                'icon' => 'lock text-red',
                            ],
                        ]
                    ],

                   ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>