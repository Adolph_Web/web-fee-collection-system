<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Mtaa */

$this->title = $model->jina;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mitaa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mtaa-view">


    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'jina',
            [
                'attribute' =>  'mtaa_baba',
                'value' => function($model){
                    if($model->mtaa_baba != null) {
                        return \backend\models\Mtaa::getNameById($model->mtaa_baba);
                    }else{
                        return '';
                    }
                }
            ],
            [
                'attribute' =>  'kata_id',
                'value' => function($model){
                    return $model->kata->jina;
                }
            ],
            'maker',
            'maker_time',
        ],
    ]) ?>

</div>
