<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mtaa */

$this->title = Yii::t('app', 'Ingiza Mtaa mpya');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mitaa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mtaa-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
