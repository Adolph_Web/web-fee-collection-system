<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mtaa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mtaa-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="card">
        <div class="card-body">
            <div class="panel panel-primary">
                <div class="row">
                    <div class="col-md-12">

                        <?= $form->field($model, 'jina')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'mtaa_baba')->dropDownList(\backend\models\Mtaa::getAll(),['prompt' => '--Chagua--']) ?>
                        <?= $form->field($model, 'kata_id')->dropDownList(\backend\models\Kata::getAll(),['prompt' => '--Chagua--']) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Weka'), ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <?php ActiveForm::end(); ?>

</div>
