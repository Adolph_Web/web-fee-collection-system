<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mlipaji */

$this->title = Yii::t('app', 'Ingiza taarifa za mlipaji');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Walipaji'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mlipaji-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
