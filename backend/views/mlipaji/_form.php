<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mlipaji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mlipaji-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jina')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chanzo_cha_mapato_id')->dropDownList(\backend\models\ChanzoChaMapato::getAll(),['prompt' => '--Chagua--']) ?>

    <?= $form->field($model, 'mtaa_id')->dropDownList(\backend\models\Mtaa::getChildAll(),['prompt' => '--Chagua--']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
