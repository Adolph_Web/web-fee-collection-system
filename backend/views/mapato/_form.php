<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mapato */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mapato-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vocha_id')->textInput() ?>

    <?= $form->field($model, 'kiasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mlipaji_id')->textInput() ?>

    <?= $form->field($model, 'chanzo_cha_malipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mfanyakazi_id')->textInput() ?>

    <?= $form->field($model, 'mtaa_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
