<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mapato */

$this->title = Yii::t('app', 'Create Mapato');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mapatos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mapato-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
