<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MapatoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mapato-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vocha_id') ?>

    <?= $form->field($model, 'kiasi') ?>

    <?= $form->field($model, 'mlipaji_id') ?>

    <?= $form->field($model, 'chanzo_cha_malipo') ?>

    <?php // echo $form->field($model, 'mfanyakazi_id') ?>

    <?php // echo $form->field($model, 'mtaa_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
