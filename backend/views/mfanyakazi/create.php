<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mfanyakazi */

$this->title = Yii::t('app', 'Create Mfanyakazi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mfanyakazis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mfanyakazi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
