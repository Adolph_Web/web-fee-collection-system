<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Vocha */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vocha-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card" style="margin-top: 10px">
        <div class="card-body">
            <div class="card-header"><strong>Fomu ya kutengeneza vocha</strong></div>
            <div class="card-body">


    <?= $form->field($model, 'kumbukumbu_namba')->textInput(['maxlength' => true,'readonly' => 'readonly']) ?>

                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'mwezi')->textInput(['maxlength' => true,'readonly' => 'readonly']) ?>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'mwaka')->textInput(['maxlength' => true,'readonly' => 'readonly']) ?>
                    </div>
                </div>

    <?= $form->field($model, 'mtaa_id')->dropDownList(\backend\models\Mtaa::getParentAll(),['prompt' => '--Chagua--']) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
