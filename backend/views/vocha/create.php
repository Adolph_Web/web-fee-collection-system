<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Vocha */

$this->title = Yii::t('app', 'Tengeneza Vocha');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vocha'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vocha-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
