<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Vocha */

$this->title = Yii::t('app', 'Update Vocha: {name}', [
    'name' => $model->kumbukumbu_namba,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vochas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vocha-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
