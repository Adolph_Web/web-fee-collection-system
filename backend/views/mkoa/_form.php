<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mkoa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mkoa-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <div class="panel panel-primary">
                <div class="row">
                    <div class="col-md-12">

                        <?= $form->field($model, 'jina')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Weka'), ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    <?php ActiveForm::end(); ?>

</div>
</div>
