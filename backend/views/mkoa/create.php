<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Mkoa */

$this->title = Yii::t('app', 'Ingiza mkoa mpya');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mkoa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mkoa-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
