<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kata".
 *
 * @property int $id
 * @property string $jina
 * @property int $wilaya_id
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Wilaya $wilaya
 * @property Mtaa[] $mtaas
 */
class Kata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kata';
    }

    public static function getAll()
    {
        return ArrayHelper::map(Kata::find()->all(),'id','jina');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina', 'wilaya_id'], 'required'],
            [['wilaya_id'], 'integer'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
            [['wilaya_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wilaya::className(), 'targetAttribute' => ['wilaya_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'wilaya_id' => Yii::t('app', 'Wilaya'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Wilaya]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWilaya()
    {
        return $this->hasOne(Wilaya::className(), ['id' => 'wilaya_id']);
    }

    /**
     * Gets query for [[Mtaas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMtaas()
    {
        return $this->hasMany(Mtaa::className(), ['kata_id' => 'id']);
    }
}
