<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vocha".
 *
 * @property int $id
 * @property int $mtaa_id
 * @property string $kumbukumbu_namba
 * @property string|null $mwezi
 * @property string|null $mwaka
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Mapato[] $mapatos
 */
class Vocha extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vocha';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kumbukumbu_namba','mtaa_id'], 'required'],
            [['maker_time'], 'safe'],
            [['mtaa_id'], 'integer'],
            [['kumbukumbu_namba', 'maker'], 'string', 'max' => 200],
            [['mwezi'], 'string', 'max' => 2],
            [['mwaka'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kumbukumbu_namba' => Yii::t('app', 'Kumbukumbu Namba'),
            'mwezi' => Yii::t('app', 'Mwezi'),
            'mwaka' => Yii::t('app', 'Mwaka'),
            'mtaa_id' => Yii::t('app', 'Mtaa'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Mapatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapatos()
    {
        return $this->hasMany(Mapato::className(), ['vocha_id' => 'id']);
    }


    public static function getLastReference()
    {
        $model = Vocha::find()->orderBy(['id' => SORT_DESC])->one();
        if($model != null){
            return 'V'.($model->id + 1).'/'.date('y').'/'.date('m').'/'.date('d');
        }else{
            return 'V1/'.date('y').'/'.date('m').'/'.date('d');
        }
    }
}
