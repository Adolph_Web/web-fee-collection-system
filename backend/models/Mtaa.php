<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mtaa".
 *
 * @property int $id
 * @property string $jina
 * @property int $kata_id
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Mapato[] $mapatos
 * @property Mlipaji[] $mlipajis
 * @property Kata $kata
 */
class Mtaa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mtaa';
    }

    public static function getAll()
    {
        return ArrayHelper::map(Mtaa::find()->all(),'id','jina');
    }

    public static function getNameById($mtaa_baba)
    {
        $mtaa = Mtaa::findOne($mtaa_baba);
        return $mtaa->jina;
    }

    public static function getParentAll()
    {
        return ArrayHelper::map(Mtaa::find()->where(['mtaa_baba' => null])->all(),'id','jina');
    }

    public static function getChildAll()
    {
        return ArrayHelper::map(Mtaa::find()->where(['!=','mtaa_baba', ''])->all(),'id','jina');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina', 'kata_id'], 'required'],
            [['kata_id','mtaa_baba'], 'integer'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
            [['kata_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kata::className(), 'targetAttribute' => ['kata_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'kata_id' => Yii::t('app', 'Kata'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Mapatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapatos()
    {
        return $this->hasMany(Mapato::className(), ['mtaa_id' => 'id']);
    }

    /**
     * Gets query for [[Mlipajis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMlipajis()
    {
        return $this->hasMany(Mlipaji::className(), ['mtaa_id' => 'id']);
    }

    /**
     * Gets query for [[Kata]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKata()
    {
        return $this->hasOne(Kata::className(), ['id' => 'kata_id']);
    }
}
