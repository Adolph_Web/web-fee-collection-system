<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mlipaji".
 *
 * @property int $id
 * @property string $jina
 * @property int $chanzo_cha_mapato_id
 * @property int $mtaa_id
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Mapato[] $mapatos
 * @property ChanzoChaMapato $chanzoChaMapato
 * @property Mtaa $mtaa
 */
class Mlipaji extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mlipaji';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina', 'chanzo_cha_mapato_id', 'mtaa_id'], 'required'],
            [['chanzo_cha_mapato_id', 'mtaa_id'], 'integer'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
            [['chanzo_cha_mapato_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChanzoChaMapato::className(), 'targetAttribute' => ['chanzo_cha_mapato_id' => 'id']],
            [['mtaa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mtaa::className(), 'targetAttribute' => ['mtaa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'chanzo_cha_mapato_id' => Yii::t('app', 'Chanzo Cha Mapato ID'),
            'mtaa_id' => Yii::t('app', 'Mtaa ID'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Mapatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapatos()
    {
        return $this->hasMany(Mapato::className(), ['mlipaji_id' => 'id']);
    }

    /**
     * Gets query for [[ChanzoChaMapato]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChanzoChaMapato()
    {
        return $this->hasOne(ChanzoChaMapato::className(), ['id' => 'chanzo_cha_mapato_id']);
    }

    /**
     * Gets query for [[Mtaa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMtaa()
    {
        return $this->hasOne(Mtaa::className(), ['id' => 'mtaa_id']);
    }
}
