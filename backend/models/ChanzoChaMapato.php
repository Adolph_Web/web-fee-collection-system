<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "chanzo_cha_mapato".
 *
 * @property int $id
 * @property string $jina
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Mlipaji[] $mlipajis
 */
class ChanzoChaMapato extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chanzo_cha_mapato';
    }

    public static function getAll()
    {
        return ArrayHelper::map(ChanzoChaMapato::find()->all(),'id' ,'jina');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina','kiasi'], 'required'],
            [['kiasi'], 'number'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'kiasi' => Yii::t('app', 'Kiasi'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Mlipajis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMlipajis()
    {
        return $this->hasMany(Mlipaji::className(), ['chanzo_cha_mapato_id' => 'id']);
    }
}
