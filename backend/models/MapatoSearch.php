<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Mapato;

/**
 * MapatoSearch represents the model behind the search form of `backend\models\Mapato`.
 */
class MapatoSearch extends Mapato
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vocha_id', 'mlipaji_id', 'mfanyakazi_id', 'mtaa_id', 'status'], 'integer'],
            [['kiasi'], 'number'],
            [['chanzo_cha_malipo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mapato::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vocha_id' => $this->vocha_id,
            'kiasi' => $this->kiasi,
            'mlipaji_id' => $this->mlipaji_id,
            'mfanyakazi_id' => $this->mfanyakazi_id,
            'mtaa_id' => $this->mtaa_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'chanzo_cha_malipo', $this->chanzo_cha_malipo]);

        return $dataProvider;
    }
}
