<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Vocha;

/**
 * VochaSearch represents the model behind the search form of `backend\models\Vocha`.
 */
class VochaSearch extends Vocha
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kumbukumbu_namba', 'mwezi', 'mwaka', 'maker', 'maker_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vocha::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'maker_time' => $this->maker_time,
        ]);

        $query->andFilterWhere(['like', 'kumbukumbu_namba', $this->kumbukumbu_namba])
            ->andFilterWhere(['like', 'mwezi', $this->mwezi])
            ->andFilterWhere(['like', 'mwaka', $this->mwaka])
            ->andFilterWhere(['like', 'maker', $this->maker]);

        return $dataProvider;
    }
}
