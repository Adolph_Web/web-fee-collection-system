<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mfanyakazi".
 *
 * @property int $id
 * @property string $jina
 * @property string|null $simu
 * @property string $kazi
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Mapato[] $mapatos
 */
class Mfanyakazi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mfanyakazi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina', 'kazi'], 'required'],
            [['maker_time'], 'safe'],
            [['jina', 'kazi', 'maker'], 'string', 'max' => 200],
            [['simu'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'simu' => Yii::t('app', 'Simu'),
            'kazi' => Yii::t('app', 'Kazi'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Mapatos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMapatos()
    {
        return $this->hasMany(Mapato::className(), ['mfanyakazi_id' => 'id']);
    }
}
