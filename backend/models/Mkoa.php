<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mkoa".
 *
 * @property int $id
 * @property string $jina
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Wilaya[] $wilayas
 */
class Mkoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mkoa';
    }

    public static function getAll()
    {
        return ArrayHelper::map(Mkoa::find()->all(),'id','jina');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina'], 'required'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Wilayas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWilayas()
    {
        return $this->hasMany(Wilaya::className(), ['mkoa_id' => 'id']);
    }
}
