<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mapato".
 *
 * @property int $id
 * @property int $vocha_id
 * @property float|null $kiasi
 * @property int $mlipaji_id
 * @property string|null $chanzo_cha_malipo
 * @property int|null $mfanyakazi_id
 * @property int|null $mtaa_id
 * @property int|null $status
 *
 * @property Mfanyakazi $mfanyakazi
 * @property Mlipaji $mlipaji
 * @property Mtaa $mtaa
 * @property Vocha $vocha
 */
class Mapato extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mapato';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vocha_id', 'mlipaji_id'], 'required'],
            [['vocha_id', 'mlipaji_id', 'mfanyakazi_id', 'mtaa_id', 'status'], 'integer'],
            [['kiasi'], 'number'],
            [['chanzo_cha_malipo'], 'string', 'max' => 200],
            [['mfanyakazi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mfanyakazi::className(), 'targetAttribute' => ['mfanyakazi_id' => 'id']],
            [['mlipaji_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mlipaji::className(), 'targetAttribute' => ['mlipaji_id' => 'id']],
            [['mtaa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mtaa::className(), 'targetAttribute' => ['mtaa_id' => 'id']],
            [['vocha_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vocha::className(), 'targetAttribute' => ['vocha_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vocha_id' => Yii::t('app', 'Vocha ID'),
            'kiasi' => Yii::t('app', 'Kiasi'),
            'mlipaji_id' => Yii::t('app', 'Mlipaji ID'),
            'chanzo_cha_malipo' => Yii::t('app', 'Chanzo Cha Malipo'),
            'mfanyakazi_id' => Yii::t('app', 'Mfanyakazi ID'),
            'mtaa_id' => Yii::t('app', 'Mtaa ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Mfanyakazi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMfanyakazi()
    {
        return $this->hasOne(Mfanyakazi::className(), ['id' => 'mfanyakazi_id']);
    }

    /**
     * Gets query for [[Mlipaji]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMlipaji()
    {
        return $this->hasOne(Mlipaji::className(), ['id' => 'mlipaji_id']);
    }

    /**
     * Gets query for [[Mtaa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMtaa()
    {
        return $this->hasOne(Mtaa::className(), ['id' => 'mtaa_id']);
    }

    /**
     * Gets query for [[Vocha]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVocha()
    {
        return $this->hasOne(Vocha::className(), ['id' => 'vocha_id']);
    }
}
