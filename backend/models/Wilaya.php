<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "wilaya".
 *
 * @property int $id
 * @property string $jina
 * @property int $mkoa_id
 * @property string|null $maker
 * @property string|null $maker_time
 *
 * @property Kata[] $katas
 * @property Mkoa $mkoa
 */
class Wilaya extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wilaya';
    }

    public static function getAll()
    {
        return ArrayHelper::map(Wilaya::find()->all(),'id','jina');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jina', 'mkoa_id'], 'required'],
            [['mkoa_id'], 'integer'],
            [['maker_time'], 'safe'],
            [['jina', 'maker'], 'string', 'max' => 200],
            [['mkoa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mkoa::className(), 'targetAttribute' => ['mkoa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jina' => Yii::t('app', 'Jina'),
            'mkoa_id' => Yii::t('app', 'Mkoa'),
            'maker' => Yii::t('app', 'Maker'),
            'maker_time' => Yii::t('app', 'Maker Time'),
        ];
    }

    /**
     * Gets query for [[Katas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKatas()
    {
        return $this->hasMany(Kata::className(), ['wilaya_id' => 'id']);
    }

    /**
     * Gets query for [[Mkoa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMkoa()
    {
        return $this->hasOne(Mkoa::className(), ['id' => 'mkoa_id']);
    }
}
